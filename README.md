# Django EcoCommons User

## Requirements


* Python >= 3.4
* Django


## Installation

```
$ pip install git+https://gitlab.com/ecocommons-australia/lib/django-ecocommons-user
```

Add the application to your project's `INSTALLED_APPS` in `settings.py`.

```
INSTALLED_APPS = [
    ...
    'ecocommons_user',
]
```

In your project's `settings.py`, set `AUTH_USER_MODEL`

```
AUTH_USER_MODEL = 'ecocommons_user.CustomUser'
```

Voila!

## Development

On any changes to the `CustomUser` model the `scripts/makemigrations.sh` helper script should be run.

## Contributing

* Please raise an issue/feature and name your branch 'feature-n' or 'issue-n', where 'n' is the issue number.
