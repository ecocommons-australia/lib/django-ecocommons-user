# Generated by Django 3.0 on 2023-07-19 07:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecocommons_user', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='customuser',
            options={},
        ),
        migrations.AddField(
            model_name='customuser',
            name='realm',
            field=models.TextField(db_index=True, default=None, null=True, verbose_name='realm'),
        )
    ]
