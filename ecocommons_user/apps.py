from django.apps import AppConfig


class EcocommonsUserConfig(AppConfig):
    name = 'ecocommons_user'
