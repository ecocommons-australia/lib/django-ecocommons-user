import os

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(BaseCommand):
    requires_system_checks = True
    help = 'Create a default admin user'

    def handle(self, *args, **options):
        try:
            email = os.getenv('DEFAULT_ADMIN_USER_EMAIL')
            password = os.getenv('DEFAULT_ADMIN_USER_PASSWORD')
            if email and password and User.objects.count() == 0:
                User.objects.create_superuser(email, password)
        except Exception as exc:
            self.stdout.write(self.style.ERROR(f'{__name__} exc'))
            raise CommandError(str(exc))

        self.stdout.write(self.style.SUCCESS(f'Successfully ran {__name__}'))
