import uuid

from django.db import models
from django.contrib.auth.models import AbstractUser

class CustomUser(AbstractUser):
    # Use uuid for pk
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    # User auth realm
    realm = models.TextField(
        'realm', 
        unique=False,
        null=True,
        default=None,
        db_index=True)