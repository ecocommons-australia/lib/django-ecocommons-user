"""Metadata for ecocommons_user package."""
__title__ = 'ecocommons_user'
__description__ = 'A user model with uuid for pk'
__url__ = 'https://gitlab.com/ecocommons-australia/lib/django-ecocommons-user'
__version__ = '0.1.0'
__author__ = 'EcoCommons Australia, Gary Burgmann'
__author_email__ = 'ecocommons@griffith.edu.au, garyburgmann@gmail.com'
__license__ = 'MIT'
