#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

docker run --rm \
    -v $SCRIPT_DIR/../:/project/ \
    --workdir /project/ \
    python:3.10-slim \
    bash -c "export PYTHONPATH=/project/ && \
                pip install django==3 && \
                python scripts/makemigrations.py"